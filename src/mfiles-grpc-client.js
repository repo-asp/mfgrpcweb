
const { AuthenticationAttemptIdentifier, LogInRequest } = require('./grpc-web-stub-generated/mfiles_pb.js');
const { IRPCLoginClient } = require('./grpc-web-stub-generated/mfiles_grpc_web_pb.js');

// Settings.
const proxyServer = "https://localhost";

const logIn = function (loginObj) {

  const loginRequest = new LogInRequest();

  loginRequest.setVersion(loginObj.version);
  loginRequest.setHostname(loginObj.hostname);
  loginRequest.setVault(loginObj.vault);
  loginRequest.setAdministrative(loginObj.administrative);
  loginRequest.setComputerName(loginObj.computer_name);
  loginRequest.setTimezone(loginObj.timezone.toString('base64'));
  loginRequest.setAuthType(loginObj.auth_type);

  var authenticationAttemptIdentifier = new AuthenticationAttemptIdentifier();
  authenticationAttemptIdentifier.setLegacyData(loginObj.login_id.legacy_data);
  authenticationAttemptIdentifier.setData(loginObj.login_id.data);

  loginRequest.setLoginId(authenticationAttemptIdentifier);
  loginRequest.setInput(loginObj.input.toString('base64'));
  loginRequest.setData(loginObj.data);

  return new Promise((resolve, reject) => {
    new IRPCLoginClient(proxyServer, null, {
      "grpc.max_metadata_size": 128 * 1024 // Allow max 128kB metadata size for error stack.
    })
      .logIn(loginRequest, null, (err, response) => {
        if (err) {
          resolve(err);
          return;
        }
        let result = camelcaseToUnderscore(response.toObject());
        result.session_info.session_id = Buffer.from(result.session_info.session_id, 'base64');
        result.session_info.timezone = Buffer.from(result.session_info.timezone, 'base64');
        console.log(result);
        resolve(result);
      })
  });

};

window.mfilesGrpcApi = {
  logIn
}

/***************** Util functions ***********************/
const camelcaseToUnderscore = (properties) => {
  try {

    const conversion = (s) => {
      return s && s.replace(/(?:^|\.?)([A-Z])/g, function (x, y) { return "_" + y.toLowerCase() }).replace(/^_/, "");
    }

    if (Array.isArray(properties)) {
      let newArray = [];
      for (let i in properties) {
        let element = properties[i];
        if (i === 'camelcaseToUnderscore')
          break;
        if (typeof element === 'string' || typeof element === 'number' || typeof element === 'boolean') {
          newArray.push(element);
        }
        else if (typeof properties === 'object') {
          let newVal = camelcaseToUnderscore(element);
          newArray.push(newVal);
        }
      }
      return newArray;
    }
    else if (typeof properties === 'object') {
      let newObject = {};
      for (let [key, value] of Object.entries(properties)) {
        if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
          newObject[conversion(key)] = value;
        }
        else newObject[conversion(key)] = camelcaseToUnderscore(value);
      }
      return newObject;
    }
    return conversion(properties);
  }
  catch (e) {
    return null;
  }
}