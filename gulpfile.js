var gulp = require('gulp');
const minify = require("gulp-minify");
const browserify = require("browserify");
const vinylSource = require("vinyl-source-stream");
const buffer = require("vinyl-buffer");

gulp.task('Build:grpc-web', function () {
    return browserify('src/mfiles-grpc-client.js')
        .bundle()
        .pipe(vinylSource("mfiles-grpc-web.js"))
        .pipe(buffer())
        .pipe(minify())
        .pipe(gulp.dest('../dist/'));
});